import subprocess
import time
import re
import json
import pickle
import tempfile
import shlex
import sys
import os
from typing import Generator, List, TextIO
from pathlib import Path

#----------------------------------------------------------------------
def cleanFile(filePath: str, startsWith: List[str] = [], skipEmpty: bool = True,
              strip: bool = False, removeInlineComments: bool = False) -> Generator:
    """open a file and yield lines, skipping empty lines
    or those that start with a given pattern"""
    if not os.path.isfile(filePath):
        raise IOError("could not find file {}".format(filePath))

    with open(filePath, 'r') as inFile:
        for line in inFile:
            #remove empty line
            if skipEmpty and not line.strip():
                continue

            #remove comment line
            lineStartsWithPattern = [True for x in startsWith if line.startswith(x)]
            if lineStartsWithPattern:
                continue

            #remove inline comments
            if removeInlineComments:
                for commentSymbol in startsWith:
                    index = line.find(commentSymbol)
                    if index > 0 and "\n" in line:
                        line = line[:index] + "\n"
                    elif index > 0:
                        line = line[:index]

            #remove newline
            if strip:
                line = line.strip()

            yield line
#----------------------------------------------------------------------
def makeIdentifier(names: List[str]) -> List[str]:
    """remove invalid identifier characters"""
    for index, name in enumerate(names):
        if not name.isidentifier():
            names[index] = re.sub(r'[^a-zA-Z][^a-zA-Z0-9]*', '_', name)

    return names

########################################################################
class Qstats:
    """Access and parse qstat.
    Provide info on the number and type of currently executing jobs"""

    #qstat variables
    _qstat = ""
    _jobNumber = 0
    _JobIDs = []
    _jobNames = []
    _idToNames = {}
    _qstatDict = {}

    #----------------------------------------------------------------------
    @classmethod
    def _reset(cls) -> None:
        """resets class variables to empty"""
        cls._qstat = ""
        cls._jobNumber = 0
        cls._JobIDs = []
        cls._jobNames = []
        cls._idToNames = {}
        cls._qstatDict = {}


    #----------------------------------------------------------------------
    @classmethod
    def _refresh(cls) -> None:
        """executes qstat and stores all the values therein"""
        #call qstat and set qstats
        cls._reset()

        for i in range(10):
            try:
                qstatOutput = subprocess.check_output(["qstat"]).decode()
            except:
                time.sleep(10)
                continue

        if qstatOutput:
            cls._qstat = qstatOutput
            qstatLineList = qstatOutput.split("\n")

            #remove header and remove separator line
            del qstatLineList[0:2]

            #parse stats and set variables
            for line in filter(None, qstatLineList):
                valuesList = line.strip().split()
                jobID = valuesList[0]
                name = valuesList[2]

                cls._JobIDs.append(jobID)
                cls._jobNames.append(name)
                cls._idToNames[jobID] = name

            cls._jobNumber = len(cls._JobIDs)
        else:
            cls._reset()

    #----------------------------------------------------------------------
    @classmethod
    def jobNumber(cls, refresh: bool = True) -> List[str]:
        """queries qstat and reports the number of ongoing jobs"""
        if refresh:
            cls._refresh()
        return cls._jobNumber

    #----------------------------------------------------------------------
    @classmethod
    def jobNames(cls, refresh: bool = True) -> List[str]:
        """query qstat and report the names of ongoing jobs"""
        if refresh:
            cls._refresh()
        return cls._jobNames

    #----------------------------------------------------------------------
    @classmethod
    def jobIDs(cls, refresh: bool = True) -> List[str]:
        """query qstat and report the IDs of ongoing jobs"""
        if refresh:
            cls._refresh()
        return cls._JobIDs




#########################################################################
#TODO: deal with submitting more than one array job, what happens to commandslistFile?
class HPCJobs:
    """handle submittal of jobs to the HPC"""
    #----------------------------------------------------------------------
    def __init__(self, reqTime: str = ":10:", reqMemory: str = "1G",
                 reqCores: str = "1", reqTempSpace: str = "50M",
                 env: dict = os.environ, tmp: str = '/hpc/tmp/') -> None:
        """Constructor"""
        #validation
        self._inputValidation(reqTime, reqMemory, reqCores, reqTempSpace)

        #variable definition
        self.env = env

        self._reqTime = reqTime
        self._reqMemory = reqMemory
        self._reqCores = reqCores
        self._reqTempSpace = reqTempSpace
        self._tmpFolder = tmp

        self.submittedJobIDs = []
        self.commandsListFile = ""  #TODO: is it necessary to have this as an attribute?

    #----------------------------------------------------------------------
    def submit(self, name: str, command: str) -> None:
        """submit a single job to the cluster"""
        #prepare command for submittal to qsub
        submitCode = 'echo '+ shlex.quote(command) + " | "

        #prepare the qsub code according to required options
        #qsub arguments
        options = (f' -shell no '
                   f'-cwd '
                   f'-V '
                   f'-q all.q '
                   f'-N {name} ')
        resources = (f'-l h_rt={self._reqTime} '
                     f'-l h_vmem={self._reqMemory} '
                     f'-l tmpspace={self._reqTempSpace} '
                     f'-pe threaded {self._reqCores} ')
        outputs = (f'-o {name}.out '
                   f'-e {name}.err ')

        #formatted qsub command
        qsubCode = ("qsub " + options + resources + outputs)

        #submitting job
        qsubOutput = subprocess.check_output(submitCode + qsubCode,
                                             shell=True, env=self.env)

        #storing job number in attribute
        qrunMatch = re.search(r"Your job (\d+) \(", qsubOutput.decode())
        self.submittedJobIDs.append(qrunMatch.group(1))

    #----------------------------------------------------------------------
    def submitArrayJob(self, name: str, commandsList: List[str]) -> None:
        """submit a list of jobs to cluster as an array job"""
        #create temp file to store the list of commandsList
        self.commandsListFile = tempfile.NamedTemporaryFile('wb', dir = self._tmpFolder)

        #pickle the list of commands into tmp
        pickle.dump(commandsList, self.commandsListFile.file)
        self.commandsListFile.file.flush()

        #prepare commandslistFile to be submitted to the HPC through subarray
        subarraySubmit = (f'echo '
                          + shlex.quote(f'subarray {self.commandsListFile.name} ')
                          + ' | ')

        jobNumber = len(commandsList)
        qsubOptions = (f'-shell no '
                       f'-cwd '
                       f'-V '
                       f'-q all.q '
                       f'-N {name} '
                       f'-t 1-{jobNumber} ')
        resources = (f' -l h_rt={self._reqTime} '
                     f' -l h_vmem={self._reqMemory} '
                     f' -l tmpspace={self._reqTempSpace} '
                     f' -pe threaded {self._reqCores} ')
        output = (f'-o {name}.out '
                  f'-e {name}.err ')

        #format qsub command
        qsubCode = ("qsub " + qsubOptions + resources + output)

        #submitting job
        qsubOutput = subprocess.check_output(subarraySubmit + qsubCode,
                                             shell=True, env=self.env)

        #storing job number in object
        qrunMatch = re.search(r"Your job-array (\d+?)\.", qsubOutput.decode())
        self.submittedJobIDs.append(qrunMatch.group(1))

    #----------------------------------------------------------------------
    def wait(self, jobWaitTime: int = 180) -> None:
        """wait for submitted jobs to finish"""
        currentJobIDs = Qstats.jobIDs()
        while [x for x in self.submittedJobIDs if x in currentJobIDs]:
            time.sleep(jobWaitTime)
            currentJobIDs = Qstats.jobIDs()

    #----------------------------------------------------------------------
    def resetJobs(self) -> None:
        """reset the IDs of submitted jobs"""
        self.submittedJobIDs = []
    #----------------------------------------------------------------------
    def _inputValidation(self, reqTime: str, reqMemory: str,
                         reqCores: str, reqTempSpace: str) -> None:
        """validate options that will be passed to qsub"""
        #patterns
        timePattern = re.compile(r"(\d*):(\d*):(\d*)")
        sizePattern = re.compile(r"\d+[GM]")

        #time
        timeMatch = timePattern.match(reqTime)
        timeNotEmpty = (timeMatch.group(1) or timeMatch.group(2) or timeMatch.group(3))
        if not (timeMatch and timeNotEmpty):
            raise SyntaxError(("required time should be in the format "
                               "hours:minutes:seconds, {} was submitted").format(reqTime))

        #memory
        memMatch = sizePattern.match(reqMemory)
        if not memMatch:
            raise SyntaxError(("required memory should be an integer followed "
                               "by either M or G, {} was submitted").format(reqMemory))
        #cores
        if not reqCores.isdigit():
            raise SyntaxError("number of cores needs to be an integer"
                              ", {} provided".format(reqCores))

        #temp space
        tempMatch = sizePattern.match(reqTempSpace)
        if not tempMatch:
            raise SyntaxError(("required temp space should be an integer followed "
                               "by either M or G, {} was submitted").format(reqMemory))


########################################################################
class PushDir:
    """Context Manager that switches cwd to the provided
    path, and switches back when done. This class should be used
    exclusively in a 'with' statement."""

    #----------------------------------------------------------------------
    def __init__(self, newPath: str, ignoreExistance: bool = False) -> None:
        """Constructor"""
        self.newPath = os.path.expanduser(newPath)
        self.ignoreExistance = ignoreExistance
        self.savedPath = None

    #----------------------------------------------------------------------
    def __enter__(self):
        """save the current directory, create the new one and move into it"""
        self.savedPath = os.getcwd()
        try:
            os.mkdir(self.newPath)
        except FileExistsError:
            if self.ignoreExistance:
                os.chdir(self.newPath)
            else:
                message = f'directory {self.newPath} already exists.'
                raise FileExistsError(message)
        else:
            os.chdir(self.newPath)

    #----------------------------------------------------------------------
    def __exit__(self, etype, value, traceback):
        """return to the previously saved directory"""
        os.chdir(self.savedPath)


#----------------------------------------------------------------------
def ePrint(*args, **kwargs):
    """a print statement directed to stderr with immediate buffer flushing"""
    print(*args, file=sys.stderr, flush = True, **kwargs)

#----------------------------------------------------------------------
def getVersion(path):
    """finds if the file is under version control and return the current commit info"""
    def runInDir(dir,cmd):
        cd = f"cd {dir} 2>/dev/null && "
        return subprocess.check_output(cd + cmd, shell=True).decode().rstrip()

    try:
        p = Path(path)
        p = p.resolve()
    except FileNotFoundError:
        return "NOT_A_FILE"
    if not p.is_file():
        return "NOT_A_FILE"
    dir = p.parents[0]
    script = p.parts[-1]
    cmd = f"git ls-files {script} 2>/dev/null"
    ls = runInDir(dir, cmd)
    if ls != script:
        return "NOT_UNDER_VERSION_CONTROL"
    cmd = "git rev-parse --abbrev-ref HEAD 2>/dev/null"
    branch = runInDir(dir,cmd)
    cmd = "git describe --match 'v[0-9]*' --tags --dirty --always 2>/dev/null"
    version = runInDir(dir,cmd)
    # strip the silly 'g' that makes it impossible to find the SHA1:
    version = re.sub(r'(.*)-(\d+)-g([a-f0-9]+)',r'\1-\2-\3',version)
    if (version == ""):
        version = "UNKNOWN"
    return branch + '_' + version

#----------------------------------------------------------------------
def readDelimitedJSON(fileName: str ) -> dict:
    """ Read JSON from a (section of) a file.
    Sections are delimited by start and end markers (needs work, must be configured/centralized).
    May raise a ValueError.
    """
    txt = ''
    start = "^#+ *[-=]* *json +start" # configure somewhere!!
    end = re.sub('start', 'end', start)
    inside = False
    with open(fileName, 'r') as file:
        for line in file:
            if re.search(start, line, flags=re.IGNORECASE):
                inside = True
                continue
            elif re.search(end, line, flags=re.IGNORECASE):
                break
            if inside:
                txt += line

    try:
        jsonObj = json.loads(txt)
    except ValueError:
        message = f'Did not find any valid JSON in {fileName}. I got: {txt}'
        raise RuntimeError(message)
    else:
        return jsonObj

#----------------------------------------------------------------------
def writeDelimitedJSON(dictToDump: dict, fileName: TextIO = sys.stderr) -> None:
    """print a dictionary in JSON format to specified a file (default stderr)"""
    print("## -------- JSON START --------", file = fileName)
    print(json.dumps(dictToDump), file = fileName)
    print('## -------- JSON END --------', file = fileName)

