import time
import re
import os
import glob
import json
import subprocess
import TUtils
import pytest

@pytest.fixture(scope = 'function')
def folderCleanup(request):
    def rmPushDirFolders():
        os.rmdir('testDir')
    request.addfinalizer(rmPushDirFolders)
    return 1


@pytest.fixture(scope = 'module', autouse = True)
def fileCleanup(request):
    def rmJobOutput():
        for file in glob.glob("TUtest*"):
            os.remove(file)
    request.addfinalizer(rmJobOutput)
    return 1

@pytest.fixture(scope = 'function')
def qdelJobs(request):
    def jobTeardown():
        job = subprocess.Popen("""qdel -u 'tcandelli'""", shell = True)
        time.sleep(5)
    request.addfinalizer(jobTeardown)
    return 1

@pytest.fixture(scope = 'class')
def jobSetup(request):
    """Constructor"""
    command = """echo 'sleep 600' | qsub -shell no -cwd -V -q all.q -N TUtest"""
    for i in range(2):
        job = subprocess.Popen(command, shell = True)
    job.wait()
    time.sleep(5)

    def jobTeardown():
        job = subprocess.Popen("""qdel -u 'tcandelli'""", shell = True)
        time.sleep(5)
    request.addfinalizer(jobTeardown)
    return 1


########################################################################
class Test_cleanFile():

    #----------------------------------------------------------------------
    def test_cleanFile_default(self, ):
        cleanFileGen = TUtils.cleanFile('cleanFileInput.txt')
        assert list(cleanFileGen) == ['#comment\n', 'text\n']

    #----------------------------------------------------------------------
    def test_cleanFile_stripNewline(self, ):
        cleanFileGen = TUtils.cleanFile('cleanFileInput.txt', strip = True)
        assert list(cleanFileGen) == ['#comment', 'text']

    #----------------------------------------------------------------------
    def test_cleanFile_retainEmptyLines(self, ):
        cleanFileGen = TUtils.cleanFile('cleanFileInput.txt', skipEmpty=False)
        assert list(cleanFileGen) == ['#comment\n', '\n', 'text\n']

    #----------------------------------------------------------------------
    def test_cleanFile_patterns(self, ):
        cleanFileGen = TUtils.cleanFile('cleanFileInput.txt', startsWith= ['#'])
        assert list(cleanFileGen) == ['text\n']

    #----------------------------------------------------------------------
    def test_cleanFile_multiPatterns(self, ):
        cleanFileGen = TUtils.cleanFile('cleanFileInput.txt', startsWith= ['#', 't'])
        assert list(cleanFileGen) == []

    #----------------------------------------------------------------------
    def test_cleanFile_all(self, ):
        cleanFileGen = TUtils.cleanFile('cleanFileInput.txt', startsWith = ['#'],
                                       strip=True)
        assert list(cleanFileGen) == ['text']

    #----------------------------------------------------------------------
    def test_cleanFile_exception(self, ):
        with pytest.raises(IOError):
            cleanFileGen = TUtils.cleanFile("nonExistentFile.txt")
            list(cleanFileGen)

    #----------------------------------------------------------------------
    def test_cleanFile_removeInlineComment(self, ):
        cleanFileGen = TUtils.cleanFile('cleanFileInputInline.txt', startsWith = ['#'],
                                        removeInlineComments = True)
        assert list(cleanFileGen) == ['text \n']


########################################################################
class Test_makeIdentifier():

    #----------------------------------------------------------------------
    def test_makeIdentifier_default(self, ):
        makeIdResult = TUtils.makeIdentifier(['valid', 'invalid/identifier'])
        assert makeIdResult == ['valid', 'invalid_identifier']



########################################################################
class Test_Qstats():

    #----------------------------------------------------------------------
    def test_Qstats_jobName(self, jobSetup):
        time.sleep(3)
        jobName = TUtils.Qstats.jobNames()
        assert jobName == ['TUtest', 'TUtest']

    #----------------------------------------------------------------------
    def test_Qstats_jobID(self, jobSetup):
        jobID = TUtils.Qstats.jobIDs()
        assert re.match("\d+", jobID[0]) and re.match("\d+", jobID[1])

    #----------------------------------------------------------------------
    def test_Qstats_jobID(self, jobSetup):
        jobNumber = TUtils.Qstats.jobNumber()
        assert jobNumber == 2



########################################################################
class Test_HPCJobs():

    #----------------------------------------------------------------------
    def test_HPCJobs_submit(self, qdelJobs):
        job = TUtils.HPCJobs()
        job.submit("TUtest", 'sleep 5')
        assert TUtils.Qstats.jobNames()[0] == 'TUtest'

    #----------------------------------------------------------------------
    def test_HPCJobs_submitArray(self, qdelJobs):
        commands = ['sleep 5', 'sleep 5']
        job = TUtils.HPCJobs()
        job.submitArrayJob('TUtest', commands)
        assert TUtils.Qstats.jobNames()[0] == 'TUtest'


    #----------------------------------------------------------------------
    def test_HPCJobs_wait(self, qdelJobs):
        job = TUtils.HPCJobs()
        job.submit("TUtest", 'sleep 5')
        job.wait(1)
        assert TUtils.Qstats.jobNames() == []

    #----------------------------------------------------------------------
    def test_HPCJobs_reset(self, qdelJobs):
        job = TUtils.HPCJobs()
        job.submit("TUtest", 'sleep 5')
        job.resetJobs()
        assert job.submittedJobIDs == []

    #----------------------------------------------------------------------
    def test_HPCJobs_timeFormat(self):
        with pytest.raises(SyntaxError):
            job = TUtils.HPCJobs(reqTime= ':::')

    #----------------------------------------------------------------------
    def test_HPCJobs_coresFormat(self):
        with pytest.raises(SyntaxError):
            job = TUtils.HPCJobs(reqCores= 'asd')

    #----------------------------------------------------------------------
    def test_HPCJobs_tempFormat(self):
        with pytest.raises(SyntaxError):
            job = TUtils.HPCJobs(reqTempSpace= '14F')

    #----------------------------------------------------------------------
    def test_HPCJobs_memFormat(self):
        with pytest.raises(SyntaxError):
            job = TUtils.HPCJobs(reqMemory= '15R')

########################################################################
class Test_pushDir():

    #----------------------------------------------------------------------
    def test_pushDir_pushing(self, folderCleanup):
        originalFolder = os.getcwd()
        with TUtils.PushDir('testDir'):
            assert os.getcwd() == originalFolder + '/testDir'

    #----------------------------------------------------------------------
    def test_pushDir_popping(self, folderCleanup):
        originalFolder = os.getcwd()
        with TUtils.PushDir('testDir'):
            pass
        assert originalFolder == os.getcwd()


    #----------------------------------------------------------------------
    def test_pushDir_existingFolder(self, folderCleanup):
        os.mkdir('testDir')
        with pytest.raises(FileExistsError):
            with TUtils.PushDir('testDir'):
                pass

    def test_pushDir_ignoreExistance(self, folderCleanup):
        os.mkdir('testDir')
        originalFolder = os.getcwd()
        with TUtils.PushDir('testDir', ignoreExistance = True):
            assert os.getcwd() == originalFolder + '/testDir'


########################################################################
class Test_json:

    def test_reading_readDelimitedJSON(self):
        jsonDict = TUtils.readDelimitedJSON('delimitedJson.txt')
        expectedDict = {"key1": "value1", "key2": "value2", "key3": "value3"}
        assert jsonDict == expectedDict

    def test_readingInvalid_readDelimitedJSON(self):
        with pytest.raises(RuntimeError):
            jsonDict = TUtils.readDelimitedJSON('cleanFileInput.txt')

    def test_writing_writeDelimitedJSON(self):
        jsonDict = {"key1": "value1", "key2": "value2", "key3": "value3"}
        outputFile = open("TUtestJsonOutput.txt", 'w')
        TUtils.writeDelimitedJSON(jsonDict, outputFile)
        outputFile.close()
        jsonRead = TUtils.readDelimitedJSON("TUtestJsonOutput.txt")
        assert jsonRead == jsonDict


